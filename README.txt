--INFORMATION--

This module provides integration with przelewy24.pl payments via sms service.
Administrators can limit access to add content only to those
who send the correct sms code.

    by malcolm - mymacroom@gmail.com


-- INSTALLATION --

Enable the module like any other Drupal Module /admin/modules


-- CONFIGURATION --

1. Go to admin/config/services/przelewy24_sms and configure your seller ID.

2. Configure options at admin/structure/types/manage/{content_type}.


-- REQUIREMENTS --

Active account on przelewy24.pl service.
